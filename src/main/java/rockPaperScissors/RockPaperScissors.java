package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {

        while (true) {

            //Roundprint
            System.out.println("Let's play round " + roundCounter);


            //Usermove gen
            String humanchoice = readInput("Your choice (Rock/Paper/Scissors)?");

            //Computermove gen
            int rand = (int)(Math.random()*3);
            String computerchoice = rpsChoices.get(rand);

            // If humanchoice.equals(computerchoice) -> "its a tie"
            if (humanchoice.equals(computerchoice)) {
                System.out.println("Human chose " + humanchoice + ", computer chose " + computerchoice + ". It's a tie" );
                System.out.println("Score: human " + humanScore + " computer " + computerScore);
            }

            // else if humanchoice > computerchoice -> "Human wins" + humanscore++
            else if (humanchoice.equals("rock") && computerchoice.equals("scissors") 
            || humanchoice.equals("paper") && computerchoice.equals("rock") 
            || humanchoice.equals("scissors") && computerchoice.equals("paper")){
                humanScore++;
                System.out.println("Human chose " + humanchoice + ", computer chose " + computerchoice + ". Human wins!" );
                System.out.println("Score: human " + humanScore + " computer " + computerScore);

            }
            // else "Computer wins" + computerScore++
            else {
                computerScore++;
                System.out.println("Human chose " + humanchoice + ", computer chose " + computerchoice + ". Computer wins!" );
                System.out.println("Score: human " + humanScore + " computer " + computerScore);
            }

            // "Do you wish to continue playing?" (y/n), if n 
            String contAnswer = readInput("Do you wish to continue playing? (y/n)?");
            if (contAnswer.equals("n")) {
                System.out.println("Bye bye :)");
                break;
            }
            else {
                roundCounter++;
            }
        }



            
        }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
